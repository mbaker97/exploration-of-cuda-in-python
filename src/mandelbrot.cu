/*

To compile:
gcc -O3  -o mandelbrot mandelbrot.c png_util.c -I. -lm -lpng

To run:
./mandelbrot

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cuda.h>

// extern "C" {
// #include "png_util.h"
// }

#define GPU_NUM 0
#define NX 16 
#define NY 16 

__global__ void mandelbrot(const int NRe, 
		const int NIm, 
		const float minRe,
		const float minIm,
		const float dRe, 
		const float dIm,
		float * __restrict__ c_count){


	int tx = threadIdx.x;
	int bx = blockIdx.x;
	int dx = blockDim.x;

	int ty = threadIdx.y;
	int by = blockIdx.y;
	int dy = blockDim.y;

	int column = tx + bx*dx;
	int row = ty + by*dy;
  
	
	if(row < NIm && column < NRe)
	{
      
      float cRe = minRe + column*dRe;
      float cIm = minIm + row*dIm;

      float zRe = 0;
      float zIm = 0;
      
      int Nt = 500;
      int t, cnt=0;
	  float zReTmp;
      for(t=0;t<Nt;++t){
	
		zReTmp = zRe*zRe - zIm*zIm + cRe;
		zIm = 2.f*zIm*zRe + cIm;
		zRe = zReTmp;

		cnt += (zRe*zRe+zIm*zIm<4.f);
      }

      c_count[column + row*NRe] = cnt;
	}
}


int main(int argc, char **argv){

  cudaSetDevice(GPU_NUM);
  const int NRe = 2048;
  const int NIm = 2048;

  /* box containing sample points */
  const float centRe = -0.89753, centIm= 0.23735;
  const float diam  = 0.151579;
  const float minRe = centRe-0.5*diam;
  const float remax = centRe+0.5*diam;
  const float minIm = centIm-0.5*diam;
  const float immax = centIm+0.5*diam;

  const float dRe = (remax-minRe)/(NRe-1.f);
  const float dIm = (immax-minIm)/(NIm-1.f);
  // printf("Params:\n %f %f %f %f %f %f %f %f %f\n", centRe, centIm, diam, minRe, remax, minIm, immax, dRe, dIm);

  float *h_count = (float*) calloc(NRe*NIm, sizeof(float));
  float *c_count;


   cudaMalloc(&c_count, NRe*NIm * sizeof(float));

  dim3 B(NX,NY);
  dim3 G((NRe+NX-1)/NX, (NIm+NY-1)/NY,1);
  mandelbrot <<<G,B>>> (NRe, NIm, minRe, minIm, dRe, dIm, c_count);
  cudaMemcpy(h_count,c_count, NRe*NIm*sizeof(float), cudaMemcpyDeviceToHost);


  // printf("elapsed time %g\n", elapsed);
  for(int i = 0;i < NRe * NIm; i++)
  {
	printf("%f\n",h_count[i]);
  }

  // FILE *png = fopen("mandelbrot_unique.png", "w");
  // write_hot_png(png, NRe, NIm, h_count, 0, 80);
  // fclose(png);

}
