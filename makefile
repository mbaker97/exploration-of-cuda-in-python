compile: src/mandelbrot.cu 
	nvcc -o bin/mandelbrot src/mandelbrot.cu #png_util.c -I. -lm -lpng

install:
	chmod +x installation/install.sh
	installation/install.sh

uninstall:
	chmod +x installation/uninstall.sh
	installation/uninstall.sh

pycuda_mandlebrot_kernel: python/pycuda_mandelbrot_kernel.py
	python3 python/pycuda_mandelbrot_kernel.py

pycuda_mandlebrot_gpuarray: python/pycuda_mandelbrot_gpuarray.py
	python3 python/pycuda_mandelbrot_gpuarray.py

cuda_python_mandelbrot: python/cuda_python_mandelbrot.py
	python3.8 python/cuda_python_mandelbrot.py

cuda_mandelbrot:
	bin/mandelbrot

compare:
	bin/mandelbrot > results/c_mandelbrot_result
	python3 python/pycuda_mandelbrot_kernel.py > results/pycuda_kernel_mandelbrot_result
	python3 python/pycuda_mandelbrot_gpuarray.py > results/pycuda_gpuarray_mandelbrot_result
	python3.8 python/cuda_python_mandelbrot.py > results/cuda_python_mandelbrot_result
	python3 python/compare.py

clean:
	rm -f bin/mandelbrot
