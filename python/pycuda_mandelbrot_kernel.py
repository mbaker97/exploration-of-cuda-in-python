import pycuda.driver as drv
from pycuda.compiler import SourceModule
import numpy

#Performs auto initialization of device and compute context
import pycuda.autoinit


mod = SourceModule("""
__global__ void mandelbrot(const int NRe, 
		const int NIm, 
		const float minRe,
		const float minIm,
		const float dRe, 
		const float dIm,
		float * __restrict__ c_count){


	int tx = threadIdx.x;
	int bx = blockIdx.x;
	int dx = blockDim.x;

	int ty = threadIdx.y;
	int by = blockIdx.y;
	int dy = blockDim.y;

	int column = tx + bx*dx;
	int row = ty + by*dy;
  
	
	if(row < NIm && column < NRe)
	{
      
      float cRe = minRe + column*dRe;
      float cIm = minIm + row*dIm;

      float zRe = 0;
      float zIm = 0;
      
      int Nt = 500;
      int t, cnt=0;
	  float zReTmp;
      for(t=0;t<Nt;++t){
	
		// z = z^2 + c
		//   = (zRe + i*zIm)*(zRe + i*zIm) + (cRe + i*cIm)
		//   = zRe^2 - zIm^2 + 2*i*zIm*zRe + cRe + i*cIm
		zReTmp = zRe*zRe - zIm*zIm + cRe;
		zIm = 2.f*zIm*zRe + cIm;
		zRe = zReTmp;

		cnt += (zRe*zRe+zIm*zIm<4.f);
      }

      c_count[column + row*NRe] = cnt;
	}
}
""")


NX = 16
NY = 16

NRe = numpy.int32(2048)
NIm = numpy.int32(2048)
#  NRe = numpy.int32(20)
#  NIm = numpy.int32(20)

centRe = numpy.float32(-0.89753)
centIm = numpy.float32(0.23735)
diam = numpy.float32(0.151579)
minRe = numpy.float32(centRe-0.5*diam)
remax = numpy.float32(centRe+0.5*diam)
minIm = numpy.float32(centIm-0.5*diam)
immax = numpy.float32(centIm+0.5*diam) 
dRe = numpy.float32((remax-minRe)/(NRe-1.0))
dIm = numpy.float32((immax-minIm)/(NIm-1.0))
# Check param values 
#  print("Params\n {:f} {:f} {:f} {:f} {:f} {:f} {:f} {:f} {:f}".format(centRe, centIm, diam, minRe, remax, minIm, immax, dRe, dIm))

dest = numpy.zeros(NRe*NIm).astype(numpy.float32)

# Run using kernel written above

mandelbrot = mod.get_function("mandelbrot")
mandelbrot(NRe, NIm, minRe, minIm, dRe, dIm, drv.InOut(dest),
        block = (NX,NY,1), 
        grid=(((NRe+NX-1)//NX).item(), ((NIm+NY-1)//NY).item(),int(1)))
for i in dest:
    print("{:.6f}".format(i))
    #  pass
