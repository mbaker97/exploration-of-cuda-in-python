#!/usr/bin/env python3
def norm_difference(x,y): if len(x) != len(y): raise ValueError("x and y are of unequal lengths")
    return sum([(a-b)**2 for a,b in zip(x,y)])
    #For Debugging norms > 0:
    #  sumation = 0.0
    #  for i in range(len(x)):
        #  local_sum = x[i] - y[i]
        #  if local_sum != 0.0:
            #  print(i)
            #  print(x[i])
            #  print(y[i])
        #  sumation += local_sum ** 2
        
        

if __name__ == "__main__":


    # Read in results as string, create array by splitting by commas
    c_result_file = open("results/c_mandelbrot_result", "r")
    c_result = c_result_file.read()
    c_entries = c_result.split("\n")

    pycuda_kernel_result_file = open("results/pycuda_kernel_mandelbrot_result", "r")
    pycuda_kernel_result = pycuda_kernel_result_file.read()
    pycuda_kernel_entries = pycuda_kernel_result.split("\n")


    pycuda_gpuarray_result_file = open("results/pycuda_gpuarray_mandelbrot_result", "r")
    pycuda_gpuarray_result = pycuda_gpuarray_result_file.read()
    pycuda_gpuarray_entries = pycuda_gpuarray_result.split("\n")


    cuda_python_result_file = open("results/cuda_python_mandelbrot_result", "r")
    cuda_python_result = cuda_python_result_file.read()
    cuda_python_entries = cuda_python_result.split("\n")


    #Remove any entries that are just whitespace or null entries
    for entry in c_entries:
        if entry.isspace() or not entry:
            c_entries.remove(entry)
            continue
        #  print(entry)

    for entry in pycuda_kernel_entries:
        if entry.isspace() or not entry:
            pycuda_kernel_entries.remove(entry)
            continue
        #  print(entry)
    for entry in pycuda_gpuarray_entries:
        if entry.isspace() or not entry:
            pycuda_gpuarray_entries.remove(entry)
            continue
        #  print(entry)
    for entry in cuda_python_entries:
        if entry.isspace() or not entry:
            cuda_python_entries.remove(entry)
            continue
        #  print(entry)

    #convert to floats:
    c_entries = [float(i) for i in c_entries]
    pycuda_kernel_entries = [float(i) for i in pycuda_kernel_entries]
    pycuda_gpuarray_entries = [float(i) for i in pycuda_gpuarray_entries]
    cuda_python_entries = [float(i) for i in cuda_python_entries]

    print("Norm difference of C and pycuda kernel: " + str(norm_difference(c_entries, pycuda_kernel_entries)))
    print("Norm difference of C and pycuda gpuarray: " + str(norm_difference(c_entries, pycuda_gpuarray_entries)))
    print("Norm difference of C and CUDA Python: " + str(norm_difference(c_entries, cuda_python_entries)))

