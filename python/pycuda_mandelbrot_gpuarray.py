import pycuda.gpuarray as gpuarray
import pycuda.driver as drv
from pycuda.compiler import SourceModule
from pycuda.elementwise import ElementwiseKernel
import numpy 

#Performs auto initialization of device and compute context
import pycuda.autoinit

NX = 16
NY = 16

NRe = numpy.int32(2048)
NIm = numpy.int32(2048)
#  NRe = numpy.int32(20)
#  NIm = numpy.int32(20)

centRe = numpy.float32(-0.89753)
centIm = numpy.float32(0.23735)
diam = numpy.float32(0.151579)
minRe = numpy.float32(centRe-0.5*diam)
remax = numpy.float32(centRe+0.5*diam)
minIm = numpy.float32(centIm-0.5*diam)
immax = numpy.float32(centIm+0.5*diam) 
dRe = numpy.float32((remax-minRe)/(NRe-1.0))
dIm = numpy.float32((immax-minIm)/(NIm-1.0))
# Check param values 
#  print("Params\n {:f} {:f} {:f} {:f} {:f} {:f} {:f} {:f} {:f}".format(centRe, centIm, diam, minRe, remax, minIm, immax, dRe, dIm))

dest = numpy.zeros(NRe*NIm).astype(numpy.float32)
mandelbrot = ElementwiseKernel(
        """const int NRe, 
            const int NIm, 
            const float minRe,
            const float minIm,
            const float dRe, 
            const float dIm,
            float * __restrict__ c_count""",

            "c_count[i] = run_mandelbrot_iterations(500, NRe, NIm, minRe, minIm, dRe, dIm, i)",
            "mandelbrot",
            preamble = '''
            __device__ int run_mandelbrot_iterations(int Nt, const int NRe, const int NIm, 
            const float minRe, const float minIm, const float dRe, const float dIm, int i)
            {
                int row = i/NRe;
                int column = i - row*NRe;

                float cRe = minRe + column * dRe;
                float cIm = minIm + row * dIm;
                int t, cnt=0;
                float zReTmp;

                float zRe = 0;
                float zIm = 0;
                for(t=0;t<Nt;++t){
                    
                    // z = z^2 + c
                    //   = (zRe + i*zIm)*(zRe + i*zIm) + (cRe + i*cIm)
                    //   = zRe^2 - zIm^2 + 2*i*zIm*zRe + cRe + i*cIm
                    zReTmp = zRe*zRe - zIm*zIm + cRe;
                    zIm = 2.f*zIm*zRe + cIm;
                    zRe = zReTmp;

                    cnt += (zRe*zRe+zIm*zIm<4.f);
                    //printf("%.6f", cnt);
                }
                //printf("row: %d, col: %d, val: %d\\n", row, column, cnt);
                return cnt;
            }
            '''
        )
gpu_array = gpuarray.to_gpu(dest)
mandelbrot(NRe, NIm, minRe, minIm, dRe, dIm, gpu_array)
gpu_array.get(ary = dest)
for i in dest:
    print("{:.6f}".format(i))
    pass

