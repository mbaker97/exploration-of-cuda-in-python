from cuda import cuda, nvrtc
import numpy as np

mandelbrot = """\

extern "C" __global__ 
void mandelbrot(const int NRe, 
		const int NIm, 
		const float minRe,
		const float minIm,
		const float dRe, 
		const float dIm,
		float * __restrict__ c_count){


	int tx = threadIdx.x;
	int bx = blockIdx.x;
	int dx = blockDim.x;

	int ty = threadIdx.y;
	int by = blockIdx.y;
	int dy = blockDim.y;

	int column = tx + bx*dx;
	int row = ty + by*dy;
  
	
	if(row < NIm && column < NRe)
	{
      
      float cRe = minRe + column*dRe;
      float cIm = minIm + row*dIm;

      float zRe = 0;
      float zIm = 0;
      
      int Nt = 500;
      int t, cnt=0;
	  float zReTmp;
      for(t=0;t<Nt;++t){
	
		zReTmp = zRe*zRe - zIm*zIm + cRe;
		zIm = 2.f*zIm*zRe + cIm;
		zRe = zReTmp;

		cnt += (zRe*zRe+zIm*zIm<4.f);
      }

      c_count[column + row*NRe] = cnt;
	}
}
        """

# Create program
err, prog = nvrtc.nvrtcCreateProgram(str.encode(mandelbrot), b"cuda_python_mandelbrot.cu", 0, [], [])

# Compile program
opts = [b"--gpu-architecture=compute_60"]
err, = nvrtc.nvrtcCompileProgram(prog, 1, opts)

# Get PTX from compilation
err, ptxSize = nvrtc.nvrtcGetPTXSize(prog)
ptx = b" " * ptxSize
err, = nvrtc.nvrtcGetPTX(prog, ptx)



# Initialize CUDA Driver API
err, = cuda.cuInit(0)

# Retrieve handle for device 0
err, cuDevice = cuda.cuDeviceGet(0)

# Create context
err, context = cuda.cuCtxCreate(0, cuDevice)


# Load PTX as module data and retrieve function
ptx = np.char.array(ptx)

err, module = cuda.cuModuleLoadData(ptx.ctypes.data)
err, kernel = cuda.cuModuleGetFunction(module, b"mandelbrot")



NUM_THREADS = 16  # Threads per block
NUM_BLOCKS = 16  # Blocks per grid
NX = 16
NY = 16

NRe = np.array([2048], dtype=np.int32)
NIm = np.array([2048], dtype=np.int32)

centRe = np.array([-0.89753], dtype=np.float32)
centIm = np.array([0.23735], dtype=np.float32)
diam = np.array([0.151579], dtype=np.float32)
minRe = np.array([centRe-0.5*diam], dtype=np.float32)
remax = np.array([centRe+0.5*diam], dtype=np.float32)
minIm = np.array([centIm-0.5*diam], dtype=np.float32)
immax = np.array([centIm+0.5*diam], dtype=np.float32) 
dRe = np.array([(remax-minRe)/(NRe-1.0)],dtype=np.float32)
dIm = np.array([(immax-minIm)/(NIm-1.0)],dtype=np.float32)

# Host count
h_count = np.zeros(NRe*NIm).astype(np.float32)

# Device count
err, c_count_class = cuda.cuMemAlloc(h_count.itemsize * h_count.size)

# Create stream
err, stream = cuda.cuStreamCreate(0)

# Copy host array to device
err, = cuda.cuMemcpyHtoDAsync(
   c_count_class, h_count.ctypes.data, h_count.itemsize * h_count.size, stream
)

c_count = np.array([int(c_count_class)], dtype=np.uint64)

args = [NRe, NIm, minRe, minIm, dRe, dIm, c_count]

#Grab pointer of all args
args = np.array([arg.ctypes.data for arg in args], dtype=np.uint64)

err, = cuda.cuLaunchKernel(
   kernel,
   (NRe+NX-1)//NX,  # grid x dim
   (NIm+NY-1)//NY,  # grid y dim
   1,  # grid z dim
   NX,  # block x dim
   NY,  # block y dim
   1,  # block z dim
   0,  # dynamic shared memory
   stream,  # stream
   args.ctypes.data,  # kernel arguments
   0,  # extra (ignore)
)

err, = cuda.cuMemcpyDtoHAsync(
   h_count.ctypes.data, c_count_class, h_count.itemsize * h_count.size, stream
)
for i in h_count:
    print("{:.6f}".format(i))
    #  pass
err, = cuda.cuStreamSynchronize(stream)

# Cleanup
err, = cuda.cuStreamDestroy(stream)
err, = cuda.cuMemFree(c_count_class)
err, = cuda.cuModuleUnload(module)
err, = cuda.cuCtxDestroy(context)
