#!/bin/bash
# Installs necessary python libraries and adds to Python Path Environment variables for demo

#Using python3.8, upgrade pip
python3.8 -m pip install --upgrade pip
# Install cuda python in python3.8
python3.8 -m pip install cuda-python

#Install pycuda in python 3.6
python3 -m pip install pycuda

#Make sure pythonpath includes local site-packages
export PYTHONPATH="${PYTHONPATH}:~/.local/lib/python3.6/site-packages/"

